package ru.tsystems.data.service;

import ru.tsystems.data.model.Person;

import java.util.List;

public interface PersonService {
    Person findPerson(String login);
    List<Person> findPersonByAge(int start, int end);
}
