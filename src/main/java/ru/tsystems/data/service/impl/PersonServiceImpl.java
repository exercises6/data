package ru.tsystems.data.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.data.dao.PersonDao;
import ru.tsystems.data.model.Person;
import ru.tsystems.data.service.PersonService;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    private final PersonDao personDao;

    public PersonServiceImpl(PersonDao personDao) {
        this.personDao = personDao;
    }

    @Override
    @Transactional(readOnly = true)
    public Person findPerson(String name) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Person> findPersonByAge(int start, int end) {
        return null;
    }
}
