package ru.tsystems.data.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import ru.tsystems.data.model.Person;
import ru.tsystems.data.service.PersonService;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequiredArgsConstructor
public class HelloWorldController {

    private final PersonService personService;

    @GetMapping("/user/{name}")
    public ResponseEntity<String> user(@PathVariable("name") String name) {
        Person person = personService.findPerson(name);
        return ResponseEntity.ok(name + " age is " + person.getAge());
    }

    @GetMapping("/user")
    public ResponseEntity<List<Person>> admin(@PathParam("start") int start,
                                              @PathParam("end") int end) {
        List<Person> personList = personService.findPersonByAge(start, end);
        return ResponseEntity.ok(personList);
    }
}
