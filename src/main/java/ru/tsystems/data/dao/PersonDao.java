package ru.tsystems.data.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.tsystems.data.model.Person;


public interface PersonDao extends JpaRepository<Person, Long> {
}
